import { 
    INIT_LOGIN,
    SIGNOUT,
    LOGIN_SUCCESS, 
    LOGIN_FAIL 
} from '../actions/types';

export const INITIAL_STATE = {
    error: null,
    isAuthenticated: false,
    user: null
}

export default (state = INITIAL_STATE, action = {}) => {
    switch (action.type) {
        case SIGNOUT: 
            return { ...state, ...INITIAL_STATE };
        case INIT_LOGIN: 
            return { ...state, ...INITIAL_STATE };
        case LOGIN_SUCCESS: 
            return { ...state, ...INITIAL_STATE, user: action.payload, isAuthenticated: true };
        case LOGIN_FAIL:
            return { ...state, ...INITIAL_STATE, error: action.payload, isAuthenticated: false };
        default: 
            return state;
    }
};
