import {
    LOBBY_JOIN_SUCCESS,
    LOBBY_JOIN_FAILED,
    MEMBER_LIST_SUCCESS,
    MEMBER_LIST_FAIL,
    INIT_LOBBY
} from '../actions/types';

export const INITIAL_STATE = {
    channelInfo: null,
    channelMembers: null,
    error: null
};

export default (state = INITIAL_STATE, action = {}) => {
    switch (action.type) {
        case INIT_LOBBY:
            return { ...state, ...INITIAL_STATE };
        case LOBBY_JOIN_SUCCESS:
            return { ...state, channelInfo: action.payload };
        case LOBBY_JOIN_FAILED:
            return { ...state, ...INITIAL_STATE, error: action.payload };
        case MEMBER_LIST_SUCCESS:
            return { ...state, channelMembers: action.payload };
        case MEMBER_LIST_FAIL:
            return { ...state, channelMembers: null };
        default:
            return state;
    }
};
