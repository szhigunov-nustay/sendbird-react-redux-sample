import {
    CREATE_GROUP_CHANNEL_SUCCESS,
    SET_ACTIVE_CHANNEL,
    MESSAGE_LIST_SUCCESS,
    SEND_MESSAGE_SUCCESS,
    INIT_CHAT,
} from '../actions/types';

export const INITIAL_STATE = {
    messages: {},
    active: null,
    activeChannel: null,
    channelList: null,
};

export default (state = INITIAL_STATE, action = {}) => {
    switch (action.type) {
        case INIT_CHAT:
            return { ...state, ...INITIAL_STATE };
        case SET_ACTIVE_CHANNEL:
            return {
                ...state,
                active: action.payload,
            };
        case CREATE_GROUP_CHANNEL_SUCCESS:
            return {
                ...state,
                active: action.payload.member,
                activeChannel: action.payload.channel,
                channelList: {
                    ...state.channelList,
                    [action.payload.member]: action.payload.channel,
                },
            };
        case MESSAGE_LIST_SUCCESS:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.payload.channel
                        .url]: action.payload.messages.reverse(),
                },
            };
        case SEND_MESSAGE_SUCCESS:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.payload.channel.url]: [
                        ...state.messages[action.payload.channel.url],
                        action.payload.message,
                    ],
                },
            };
        default:
            return state;
    }
};
