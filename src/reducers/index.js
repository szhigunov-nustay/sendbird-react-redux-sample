import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import login from './login';
import lobby from './lobby';
import chat from './chat';

export default combineReducers({
    login,
    lobby,
    chat,
    router,
});
