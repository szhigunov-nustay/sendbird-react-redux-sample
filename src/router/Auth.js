import React, { Component } from 'react';
import { connect } from 'react-redux';
import { initLogin, sendbirdLogin } from '../actions';

class Auth extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            email: null,
            nickname: null,
        };
    }
    componentDidMount() {
        const { user } = this.props;
        if (!user) {
            this.props.initLogin();
            this.setState({ isLoading: false });
        } else {
            this.setState({
                isLoading: true,
                email: user.userId,
                nickname: user.nickname,
            });
            this.props.sendbirdLogin({
                userId: user.userId,
                nickname: user.nickname,
            });
        }
    }
    componentWillReceiveProps(props) {
        let { user, error } = props;
        if (user) {
            this.setState({
                isLoading: false,
            });
            console.log('got user', this);
        } else {
            this.setState({ isLoading: false });
            this.props.initLogin();
        }
        if (error) {
            this.setState({ isLoading: false });
        }
    }
    onSignInClick() {
        this.props.sendbirdLogin({
            userId: this.state.email,
            nickname: this.state.nickname,
        });
    }
    onFormChange(name, e) {
        const value = e.target.value;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <div>
                <h3>Login</h3>
                <input
                    onChange={this.onFormChange.bind(this, 'email')}
                    type="email"
                    defaultValue={this.props.user && this.props.user.userId}
                    placeholder="Your email"
                />
                <br />
                <input
                    onChange={this.onFormChange.bind(this, 'nickname')}
                    type="text"
                    defaultValue={this.props.user && this.props.user.nickname}
                    placeholder="Your nickname"
                />
                <br />
                {!this.state.isLoading && (
                    <button
                        disabled={!(this.state.email && this.state.nickname)}
                        onClick={this.onSignInClick.bind(this)}
                    >
                        Sign In
                    </button>
                )}
                {this.state.isLoading && <div>...Loading...</div>}
            </div>
        );
    }
}

function mapStateToProps({ login }) {
    const { user, error, isAuthenticated } = login;
    return { user, error, isAuthenticated };
}

export default connect(mapStateToProps, { initLogin, sendbirdLogin })(Auth);
