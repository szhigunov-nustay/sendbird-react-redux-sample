import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signOut, initLobby } from '../actions';

import Sidebar from '../containers/Sidebar';
import ChatWindow from '../containers/ChatWindow';

class Main extends Component {
    constructor() {
        super();
        this.onClickSignout = this.onClickSignout.bind(this);
    }
    componentDidMount(){
        this.props.initLobby();
    }
    onClickSignout() {
        this.props.signOut();
    }
    render() {
        const { nickname } = this.props;

        return (
            <div className="row" style={{ height: '100%' }}>
                <div className="navbar-fixed">
                    <nav>
                        <div className="nav-wrapper">
                            <ul className="right hide-on-med-and-down">
                                <li>
                                    <a>{nickname}</a>
                                    <a
                                        className="waves-effect waves-light btn"
                                        onClick={this.onClickSignout}
                                    >
                                        Sign Out
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="body" style={{ height: '100%' }}>
                    <div
                        className="card-panel col s4"
                        style={{ height: '100%' }}
                    >
                        <Sidebar />
                    </div>
                    <div
                        className="card-panel col s8"
                        style={{ height: '100%' }}
                    >
                        <ChatWindow />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps({ login: { user: { nickname } } }) {
    return { nickname };
}

export default connect(mapStateToProps, { signOut, initLobby })(Main);
