import React from 'react';

import createHistory from 'history/createHashHistory';
import { Route, Switch } from 'react-router';

import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import AuthRoute from '../components/AuthRoute';

import Main from './Main';
import Auth from './Auth';
// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const _middleware = routerMiddleware(history);

const _ConnectedRouter = () => (
    <ConnectedRouter history={history}>
        <Switch>
            <AuthRoute exact path="/" component={Main} />
            <Route path="/login" component={Auth} />
        </Switch>
    </ConnectedRouter>
);

export const middleware = _middleware;
export default _ConnectedRouter;
