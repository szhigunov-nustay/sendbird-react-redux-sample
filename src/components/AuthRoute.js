import React from 'react';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';

const AuthRoute = ({ component: Component, isAuthenticated, ...rest }) => {
    return (
        <Route
            {...rest}
            render={(props) => {
                return isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: props.location },
                        }}
                    />
                );
            }}
        />
    );
};

function mapStateToProps({ login: { isAuthenticated } }) {
    return { isAuthenticated };
}

export default connect(mapStateToProps, {})(AuthRoute);
