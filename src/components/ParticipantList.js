import React, { Component } from 'react';
import { connect } from 'react-redux';
import { joinLobby, getParticipantList } from '../actions';
import { createChat, setActiveChat } from '../actions/chat';

class ParticipantList extends Component {
    componentDidMount() {
        this.props.getParticipantList(this.props.channel);
    }
    onOpenChatClick(member) {
        this.props.createChat(member);
    }
    render() {
        return (
            <div>
                <span>Participants</span>
                {this.props.members ? (
                    <ul className="collection">
                        {this.props.members.map((member, index) => (
                            <a
                                className="collection-item"
                                key={index}
                                onClick={() => this.onOpenChatClick(member)}
                            >
                                <img
                                    src={member.profileUrl}
                                    width="16px"
                                    height="16px"
                                />
                                <span>{member.nickname}</span>
                            </a>
                        ))}
                    </ul>
                ) : (
                    <div>No members</div>
                )}
            </div>
        );
    }
}

function mapStateToProps({ lobby }) {
    return {
        channel: lobby.channelInfo,
        members: lobby.channelMembers,
    };
}

export default connect(mapStateToProps, {
    joinLobby,
    getParticipantList,
    setActiveChat,
    createChat,
})(ParticipantList);
