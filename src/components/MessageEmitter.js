import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sendMessage } from '../actions/chat';

class MessageEmitter extends Component {
    constructor() {
        super();
        this.state = { text: '' };
    }
    sendMessage() {
        if (this.state.text.length > 0) {
            this.props.sendMessage(this.props.channel, this.state.text);
            this.setState({ text: '' });
        }
    }
    onTextChange(e) {
        const value = e.target.value;
        this.setState({ text: value });
    }
    render() {
        if (!this.props.channel) {
            return null;
        }
        return (
            <div className="row">
                <form className="col s12">
                    <div className="input-field col s9">
                        <textarea
                            value={this.state.text}
                            onChange={(e) => this.onTextChange(e)}
                            className="materialize-textarea"
                        />
                    </div>
                    <div className="input-field col s3">
                        <a
                            onClick={() => this.sendMessage()}
                            className="waves-effect waves-light btn"
                        >
                            send
                        </a>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(
    ({ chat }) => ({
        active: chat.active,
        channel: chat.activeChannel,
    }),
    { sendMessage }
)(MessageEmitter);
