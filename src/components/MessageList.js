import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMessages, initChat } from '../actions/chat';

class MessageList extends Component {
    componentDidMount() {
        this.props.initChat()
    }

    componentWillReceiveProps(props) {
        const { channel } = props;
        if (channel !== this.props.channel && channel) {
            this.props.getMessages(channel);
        }
    }
    render() {
        let messages = null;
        if (this.props.channel) {
            messages = this.props.messages[this.props.channel.url];
            // if (messages) messages = messages.reverse();
        }

        return (
            <div className="row">
                {this.props.active ? (
                    <div className="col s12">
                        <span>Chat with {this.props.active}</span>
                        <ul className="collection">
                            {messages ? (
                                messages.map(
                                    ({
                                        message_id,
                                        message,
                                        _sender: user,
                                    }) => {
                                        return (
                                            <li
                                                key={message_id}
                                                className="collection-item"
                                            >
                                                {user.nickname}:{message}
                                            </li>
                                        );
                                    }
                                )
                            ) : (
                                <li className="collection-item">
                                    Start typing something...
                                </li>
                            )}
                        </ul>
                    </div>
                ) : (
                    <div className="col s12">
                        <p className="text-center">Choose a chat</p>
                    </div>
                )}
            </div>
        );
    }
}

export default connect(
    (state) => {
        return {
            messages: state.chat.messages,
            active: state.chat.active,
            channel: state.chat.activeChannel,
        };
    },
    { getMessages, initChat }
)(MessageList);
