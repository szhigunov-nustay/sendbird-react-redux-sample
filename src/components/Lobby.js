import React, { Component } from 'react';
import { connect } from 'react-redux';
import { joinLobby, initLobby } from '../actions';
import Loading from './Loading';
import ParticipantList from './ParticipantList';

class Lobby extends Component {
    constructor() {
        super();
        this.state = { isLoading: true };
    }
    componentDidMount() {
        if (!this.props.roomName) {
            this.props.joinLobby();
        }
    }
    componentWillUnmount() {
        this.props.initLobby();
    }
    componentWillReceiveProps(props) {
        if (props.roomName) {
            this.setState({
                isLoading: false,
            });
        }
    }
    render() {
        const { roomName, roomParticipantCount } = this.props;
        if (this.state.isLoading) {
            return (
                <div>
                    <Loading />
                </div>
            );
        }
        return (
            <div>
                <span>
                    {roomName} - (<span>{roomParticipantCount}</span>)
                </span>
                <ParticipantList />
            </div>
        );
    }
}

function mapStateToProps({ lobby }) {
    return {
        roomName: lobby.channelInfo && lobby.channelInfo.name,
        roomParticipantCount:
            lobby.channelInfo && lobby.channelInfo.participantCount,
    };
}

export default connect(mapStateToProps, { joinLobby, initLobby })(Lobby);
