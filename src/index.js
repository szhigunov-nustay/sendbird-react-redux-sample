/* global document */
import ReactDOM from 'react-dom';
import React from 'react';

global.APP_ID = '1E5FD126-0427-4F35-B05F-485583FBB557';

import App from './App';

ReactDOM.render(<App /> , document.getElementById('app'));