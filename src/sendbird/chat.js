import SendBird from 'sendbird';

export const sbCreateGroupChannel = (inviteUserIdList, isDistinct) => {
    return new Promise((resolve, reject) => {
        const sb = SendBird.getInstance();
        sb.GroupChannel.createChannelWithUserIds(
            inviteUserIdList,
            isDistinct,
            (channel, error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(channel);
                }
            }
        );
    });
};

export const sbCreatePreviousMessageListQuery = (channel) => {
    return channel.createPreviousMessageListQuery();
};

export const sbGetMessageList = (previousMessageListQuery) => {
    const limit = 30;
    const reverse = true;
    return new Promise((resolve, reject) => {
        previousMessageListQuery.load(limit, reverse, (messages, error) => {
            if (error) {
                reject(error);
            } else {
                resolve(messages);
            }
        });
    });
};

export const sbSendMessage = (channel, message) => {
    return new Promise((resolve, reject) => {
        channel.sendUserMessage(message, (message, error) => {
            if (error) {
                console.error(error);
                reject(error);
            }

            // onSent
            resolve(message);
        });
    });
};
