import React, { Component } from 'react';
import Lobby from '../components/Lobby';

class Sidebar extends Component {
    render() {
        return (
            <div className="row" style={{height: '100%'}}>
                <div className="col">
                    <Lobby />
                </div>
            </div>
        );
    }
}

export default Sidebar;
