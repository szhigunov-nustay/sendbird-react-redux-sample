import React, { Component } from 'react';
import MessageEmitter from '../components/MessageEmitter';
import MessageList from '../components/MessageList';

class ChatWindow extends Component {
    render() {
        return (
            <div className="row" style={{ height: '100%' }}>
                <div
                    className="col s12"
                    style={{
                        height: '85%',
                    }}
                >
                    <MessageList />
                </div>
                <div className="col s12" style={{ height: '15%' }}>
                    <MessageEmitter />
                </div>
            </div>
        );
    }
}

export default ChatWindow;
