/* global localStorage */
/* eslint no-console: 0 */
export const loadUser  = () => {
        try {
            const serializedUser = localStorage.getItem('user');

            if ( !serializedUser ) {
                return undefined;
            } else {
                return JSON.parse(serializedUser)
            }
        } catch (e) {
            // Ignoring
            console.log('failed to read user from localStorage');
        }
}

export const saveUser = (state) => {
    const user = state.login.user;
    try {
        const serializedUser = JSON.stringify(user);

        localStorage.setItem('user', serializedUser);
    } catch (e) {
        // Ignoring
        // console.log(e);
        console.log('failed to serialize user');
    }
}
