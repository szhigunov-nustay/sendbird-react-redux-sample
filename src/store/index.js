import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
/* global window */
import { middleware as routerMiddleware } from '../router';
import { saveUser, loadUser } from './localStorage';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const localStorageUserData = loadUser();
const store = createStore(
    reducers,
    {
        login: {
            user: localStorageUserData,
            isAuthenticated: false
        }
    },
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware)
    )
);

store.subscribe(() => {
    saveUser(store.getState());
});

export default store;