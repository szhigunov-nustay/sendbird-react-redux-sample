// User
export const INIT_LOGIN = 'init_login';
export const SIGNOUT = 'signout';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_FAIL = 'login_fail';

export const INIT_LOBBY = 'init_lobby';
export const INIT_CHAT = 'init_chat';
export const DISCONNECT_SUCCESS = 'disconnect_success';

export const INIT_PROFILE = 'init_profile';
export const GET_PROFILE_SUCCESS = 'get_profile_success';
export const UPDATE_PROFILE_SUCCESS = 'update_profile_success';
export const UPDATE_PROFILE_FAIL = 'update_profile_fail';

// Open Channel
export const LOBBY_JOIN_SUCCESS = 'lobby_join_success';
export const LOBBY_JOIN_FAILED = 'lobby_join_failed';
export const INIT_OPEN_CHANNEL = 'init_open_channel';
export const INIT_OPEN_CHANNEL_CREATE = 'init_open_channel_create';
export const OPEN_CHANNEL_CREATE_SUCCESS = 'open_channel_create_success';
export const OPEN_CHANNEL_CREATE_FAIL = 'open_channel_create_fail';

// Group Channel
export const INIT_GROUP_CHANNEL = 'init_group_channel';
export const GROUP_CHANNEL_PROGRESS_START = 'group_channel_progress_start';
export const GROUP_CHANNEL_PROGRESS_END = 'group_channel_progress_end';
export const GROUP_CHANNEL_LIST_SUCCESS = 'group_channel_list_success';
export const GROUP_CHANNEL_LIST_FAIL = 'group_channel_list_fail';
export const GET_GROUP_CHANNEL_SUCCESS = 'get_group_channel_success';
export const GET_GROUP_CHANNEL_FAIL = 'get_group_channel_fail';
export const CHANNEL_EDIT_SUCCESS = 'channel_edit_success';
export const CHANNEL_EDIT_FAIL = 'channel_edit_fail';
export const ADD_GROUP_CHANNEL_ITEM = 'add_group_channel_item';
export const CLEAR_SELECTED_GROUP_CHANNEL = 'clear_selected_group_channel';

export const INIT_INVITE = 'init_invite';
export const USER_LIST_SUCCESS = 'user_list_success';
export const USER_LIST_FAIL = 'user_list_fail';
export const CREATE_GROUP_CHANNEL_SUCCESS = 'create_group_channel_success';
export const CREATE_GROUP_CHANNEL_FAIL = 'create_group_channel_fail';
export const INVITE_GROUP_CHANNEL_SUCCESS = 'invite_group_channel_success';
export const INVITE_GROUP_CHANNEL_FAIL = 'invite_group_channel_fail';

export const GROUP_CHANNEL_CHANGED = 'group_channel_changed';

// Chat
export const INIT_CHAT_SCREEN = 'init_chat_screen';
export const CREATE_CHAT_HANDLER_SUCCESS = 'create_chat_handler_success';
export const CREATE_CHAT_HANDLER_FAIL = 'create_chat_handler_fail';
export const CHANNEL_TITLE_CHANGED = 'channel_title_changed';
export const CHANNEL_TITLE_CHANGED_FAIL = 'channel_title_changed_fail';
export const MESSAGE_LIST_SUCCESS = 'message_list_success';
export const MESSAGE_LIST_FAIL = 'message_list_fail';
export const SEND_MESSAGE_TEMPORARY = 'send_message_temporary';
export const SEND_MESSAGE_SUCCESS = 'send_message_success';
export const SEND_MESSAGE_FAIL = 'send_message_fail';

export const SEND_TYPING_START_SUCCESS = 'send_typing_start_success';
export const SEND_TYPING_START_FAIL = 'send_typing_start_fail';
export const SEND_TYPING_END_SUCCESS = 'send_typing_end_success';
export const SEND_TYPING_END_FAIL = 'send_typing_end_fail';

export const CHANNEL_EXIT_SUCCESS = 'channel_exit_success';
export const CHANNEL_EXIT_FAIL = 'channel_exit_fail';

export const MESSAGE_RECEIVED = 'message_received';
export const MESSAGE_UPDATED = 'message_updated';
export const MESSAGE_DELETED = 'message_deleted';
export const CHANNEL_CHANGED = 'channel_changed';
export const TYPING_STATUS_UPDATED = 'typing_status_updated';
export const READ_RECEIPT_UPDATED = 'read_receipt_updated';

// Member/Participant
export const INIT_MEMBER = 'init_member';
export const MEMBER_LIST_SUCCESS = 'member_list_success';
export const MEMBER_LIST_FAIL = 'member_list_fail';

export const SET_ACTIVE_CHANNEL = 'set_active_channel';
