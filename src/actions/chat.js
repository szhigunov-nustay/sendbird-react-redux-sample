import {
    CREATE_GROUP_CHANNEL_SUCCESS,
    CREATE_GROUP_CHANNEL_FAIL,
    SET_ACTIVE_CHANNEL,
    MESSAGE_LIST_SUCCESS,
    SEND_MESSAGE_SUCCESS,
    INIT_CHAT,
} from './types';
import {
    sbCreatePreviousMessageListQuery,
    sbGetMessageList,
    sbSendMessage,
    sbCreateGroupChannel,
} from '../sendbird';

export const initChat = () => {
    return {
        type: INIT_CHAT
    }
};

export const createChat = ({ userId }) => {
    return (dispatch) => {
        sbCreateGroupChannel([userId], true)
            .then((channel) => {
                dispatch({
                    type: CREATE_GROUP_CHANNEL_SUCCESS,
                    payload: {
                        member: userId,
                        channel: channel,
                    },
                });
            })
            .catch((e) => {
                dispatch({
                    type: CREATE_GROUP_CHANNEL_FAIL,
                    payload: e,
                });
            });
    };
};

export const setActiveChat = ({ userId }) => {
    return (dispatch) => {
        dispatch({
            type: SET_ACTIVE_CHANNEL,
            payload: userId,
        });
    };
};

export const getMessages = (channel) => {
    return (dispatch) => {
        const query = sbCreatePreviousMessageListQuery(channel);
        sbGetMessageList(query)
            .then((messages) => {
                dispatch({
                    type: MESSAGE_LIST_SUCCESS,
                    payload: {
                        channel,
                        messages,
                    },
                });
            })
            .catch((e) => {
                console.log(e);
            });
    };
};

export const sendMessage = (channel, message) => {
    return (dispatch) => {
        sbSendMessage(channel, message)
            .then((message) => {
                dispatch({
                    type: SEND_MESSAGE_SUCCESS,
                    payload: {
                        channel,
                        message,
                    },
                });
            })
            .catch(() => {
                // Error.
            });
    };
};
