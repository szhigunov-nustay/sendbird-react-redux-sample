import {
    LOBBY_JOIN_SUCCESS,
    MEMBER_LIST_SUCCESS,
    MEMBER_LIST_FAIL,
    LOBBY_JOIN_FAILED,
    INIT_LOBBY
} from './types';
import {
    sbGetOpenChannel,
    sbOpenChannelEnter,
    sbGetParticipantList,
} from '../sendbird';


export const initLobby = () => {
    return { type: INIT_LOBBY };
};
export const joinLobby = () => {
    return (dispatch) => {
        sbGetOpenChannel('common')
            .then(sbOpenChannelEnter)
            .then((channel) => {
                dispatch({
                    type: LOBBY_JOIN_SUCCESS,
                    payload: channel,
                });
            })
            .catch(() => {
                dispatch({
                    type: LOBBY_JOIN_FAILED,
                });
            });
    };
};

export const getParticipantList = (channel) => {
    return (dispatch) => {
        const query = channel.createParticipantListQuery();

        sbGetParticipantList(query)
            .then((participants) => {
                dispatch({
                    type: MEMBER_LIST_SUCCESS,
                    payload: participants,
                });
            })
            .catch((e) => {
                dispatch({
                    type: MEMBER_LIST_FAIL,
                    payload: e,
                });
            });
    };
};
