/* global localStorage */
import { INIT_LOGIN, SIGNOUT, LOGIN_SUCCESS, LOGIN_FAIL } from './types';
import { push } from 'react-router-redux';
import { sbConnect } from '../sendbird';

export const initLogin = () => {
    return { type: INIT_LOGIN };
};

export const signOut = () => {
    return (dispatch) => {
        localStorage.removeItem('user');
        signOutSuccess(dispatch);
    };
};

export const sendbirdLogin = ({ userId, nickname }) => {
    return (dispatch) => {
        sbConnect(userId, nickname)
            .then((user) => {
                loginSuccess(dispatch, user);
                dispatch(push('/'));
            })
            .catch((error) => {
                loginFail(dispatch, error);
            });
    };
};

const signOutSuccess = (dispatch) => {
    dispatch({
        type: SIGNOUT,
    });
};

const loginFail = (dispatch, error) => {
    dispatch({
        type: LOGIN_FAIL,
        payload: error,
    });
};

const loginSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_SUCCESS,
        payload: user,
    });
};
