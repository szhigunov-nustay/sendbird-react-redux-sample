# Sendbird React/Redux sample

## How to launch
* `npm i`
* `npm run build`
* serve folder with any server (I prefer `node-static` https://www.npmjs.com/package/node-static )
* to run test use `npm run test`
## Basic Functionality

* Login/Logout
* Lobby Channel with user lists
* Basic 1-to-1 chat

## Tests

Added few unit-tests with Jest, which cover all common cases in business logic

* App rendering 
* Reducers
* Actions

## Commentaries

Sendbird.com offers good documentation with a lot of examples, 
but I decided to start with my own API implementation (partially).

Overall that application could be improved with following functionality:
*   Watch for real-time changes ( new users, new messages )
*   Implement additional auth layer to provide signup with secure password ( for example with auth0 )
*   UI Improvements ( Login Screen, Message box, etc. )
