import { Reducer } from 'redux-testkit';
import * as types from '../../../src/actions/types';
import reducer from '../../../src/reducers/login';

describe('store/reducer/login', () => {
    
    it('should have initial state', () => {
        expect(reducer()).toEqual({
            error: null,
            isAuthenticated: false,
            user: null
        })
    });

    it('should handle SIGNOUT with initial state', () => {
        const action = { type: types.SIGNOUT };

        Reducer(reducer).expect(action).toReturnState({
            error: null,
            isAuthenticated: false,
            user: null
        })
    });

    it('should handle INIT_LOGIN with initial state', () => {
        const action = { type: types.INIT_LOGIN };

        Reducer(reducer).expect(action).toReturnState({
            error: null,
            isAuthenticated: false,
            user: null
        })

    });

    it('should handle LOGIN_SUCCESS with initial state', () => {
        const action = { type: types.LOGIN_SUCCESS, payload: {} };

        Reducer(reducer).expect(action).toReturnState({
            error: null,
            isAuthenticated: true,
            user: {}
        })

    });

    it('should handle LOGIN_FAIL with initial state', () => {
        const action = { type: types.LOGIN_FAIL, payload: {} };

        Reducer(reducer).expect(action).toReturnState({
            error: {},
            isAuthenticated: false,
            user: null
        })

    });
})