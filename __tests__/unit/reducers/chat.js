import { Reducer } from 'redux-testkit';
import * as types from '../../../src/actions/types';
import reducer from '../../../src/reducers/chat';

describe('store/reducer/chat', () => {
    it('should have initial state', () => {
        expect(reducer()).toEqual({
            messages: {},
            active: null,
            activeChannel: null,
            channelList: null,
        });
    });

    it('should handle SET_ACTIVE_CHANNEL with initial state', () => {
        const action = { type: types.SET_ACTIVE_CHANNEL, payload: 'testUser' };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                messages: {},
                active: 'testUser',
                activeChannel: null,
                channelList: null,
            });
    });

    it('should handle CREATE_GROUP_CHANNEL_SUCCESS with initial state', () => {
        const action = {
            type: types.CREATE_GROUP_CHANNEL_SUCCESS,
            payload: {
                channel: { url: 'channelUrl' },
                member: 'testUser',
            },
        };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                messages: {},
                active: 'testUser',
                activeChannel: { url: 'channelUrl' },
                channelList: {
                    testUser: { url: 'channelUrl' },
                },
            });
    });

    it('should handle MESSAGE_LIST_SUCCESS with initial state', () => {
        const action = {
            type: types.MESSAGE_LIST_SUCCESS,
            payload: {
                channel: { url: 'channelUrl' },
                messages: [{}, {}, {}],
            },
        };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                messages: {
                    channelUrl: [{}, {}, {}],
                },
                active: null,
                activeChannel: null,
                channelList: null,
            });
    });

    it('should handle SEND_MESSAGE_SUCCESS', () => {
        const action = {
            type: types.SEND_MESSAGE_SUCCESS,
            payload: {
                channel: { url: 'channelUrl' },
                message: { text: 'Hello World' },
            },
        };

        Reducer(reducer)
            .withState({
                active: null,
                activeChannel: { url: 'channelUrl' },
                channelList: null,
                messages: {
                    channelUrl: [{ text: 'Hello Initial' }],
                },
            })
            .expect(action)
            .toReturnState({
                messages: {
                    channelUrl: [
                        { text: 'Hello Initial' },
                        { text: 'Hello World' },
                    ],
                },
                active: null,
                activeChannel: { url: 'channelUrl' },
                channelList: null,
            });
    });

    it('should handle INIT_CHAT', () => {
        const action = { type: types.INIT_CHAT };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                messages: {},
                active: null,
                activeChannel: null,
                channelList: null,
            });
    });
});
