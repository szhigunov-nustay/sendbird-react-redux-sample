import { Reducer } from 'redux-testkit';
import * as types from '../../../src/actions/types';
import reducer from '../../../src/reducers/lobby';

describe('store/reducer/lobby', () => {
    it('should have initial state', () => {
        expect(reducer()).toEqual({
            channelInfo: null,
            channelMembers: null,
            error: null,
        });
    });

    it('should handle LOBBY_JOIN_SUCCESS with initial state', () => {
        const action = { type: types.LOBBY_JOIN_SUCCESS, payload: {} };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                channelMembers: null,
                channelInfo: {},
                error: null,
            });
    });

    it('should handle LOBBY_JOIN_FAILED with initial state', () => {
        const action = { type: types.LOBBY_JOIN_FAILED, payload: {} };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                channelMembers: null,
                channelInfo: null,
                error: {},
            });
    });

    it('should handle MEMBER_LIST_SUCCESS with initial state', () => {
        const action = { type: types.MEMBER_LIST_SUCCESS, payload: [{}] };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                channelMembers: [{}],
                channelInfo: null,
                error: null,
            });
    });

    it('should handle MEMBER_LIST_FAIL with initial state', () => {
        const action = { type: types.MEMBER_LIST_FAIL };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                channelMembers: null,
                channelInfo: null,
                error: null,
            });
    });

    it('should handle INIT_LOBBY', () => {
        const action = { type: types.INIT_LOBBY };
        Reducer(reducer)
            .expect(action)
            .toReturnState({
                channelInfo: null,
                channelMembers: null,
                error: null,
            });
    });
});
