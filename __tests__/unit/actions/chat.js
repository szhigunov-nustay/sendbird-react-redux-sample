import * as ChatActions from '../../../src/actions/chat';
import { Thunk } from 'redux-testkit';
import {
    sbCreatePreviousMessageListQuery,
    sbGetMessageList,
    sbSendMessage,
    sbCreateGroupChannel,
} from '../../../src/sendbird/chat';
jest.mock('../../../src/sendbird/chat');

import {
    CREATE_GROUP_CHANNEL_SUCCESS,
    SET_ACTIVE_CHANNEL,
    MESSAGE_LIST_SUCCESS,
    SEND_MESSAGE_SUCCESS,
} from '../../../src/actions/types';

describe('store/chat/actions', () => {
    const channel = { url: 'channelURL' };
    beforeEach(() => {
        jest.resetAllMocks();
    });
    it('should create 1-to-1 chat', async () => {
        sbCreateGroupChannel.mockResolvedValue(channel);
        const dispatches = await Thunk(ChatActions.createChat).execute({
            userId: '111',
        });

        expect(dispatches[0].getAction()).toEqual({
            type: CREATE_GROUP_CHANNEL_SUCCESS,
            payload: {
                member: '111',
                channel: channel,
            },
        });
    });

    it('should create change active chat', async () => {
        const dispatches = await Thunk(ChatActions.setActiveChat).execute({
            userId: '111',
        });

        expect(dispatches[0].getAction()).toEqual({
            type: SET_ACTIVE_CHANNEL,
            payload: '111',
        });
    });

    it('should create fetch active chat messages', async () => {
        sbCreatePreviousMessageListQuery.mockReturnValue({});
        sbGetMessageList.mockResolvedValue([{}, {}, {}]);

        const dispatches = await Thunk(ChatActions.getMessages).execute(
            channel
        );

        expect(dispatches[0].getAction()).toEqual({
            type: MESSAGE_LIST_SUCCESS,
            payload: {
                channel: channel,
                messages: [{}, {}, {}],
            },
        });
    });

    it('should create send messages to active chat', async () => {
        sbSendMessage.mockResolvedValue({});

        const dispatches = await Thunk(ChatActions.sendMessage).execute(
            channel,
            'test text'
        );

        expect(dispatches[0].getAction()).toEqual({
            type: SEND_MESSAGE_SUCCESS,
            payload: {
                channel: channel,
                message: {},
            },
        });
    });
});
