import * as LobbyActions from '../../../src/actions/';
import { Thunk } from 'redux-testkit';

import {
    LOBBY_JOIN_SUCCESS,
    MEMBER_LIST_SUCCESS,
    MEMBER_LIST_FAIL,
    LOBBY_JOIN_FAILED,
} from '../../../src/actions/types';

import {
    sbGetOpenChannel,
    sbOpenChannelEnter,
    sbGetParticipantList,
} from '../../../src/sendbird/openChannel';

jest.mock('../../../src/sendbird/openChannel');

describe('store/lobby/actions', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should join the lobby channel', async () => {
        const mockChannel = {
            name: 'Common',
            url: 'common',
        };

        mockChannel.createParticipantListQuery = jest.fn();

        sbGetOpenChannel.mockResolvedValue(mockChannel);

        sbOpenChannelEnter.mockResolvedValue(mockChannel);

        const dispatches = await Thunk(LobbyActions.joinLobby).execute(
            mockChannel
        );

        expect(dispatches).toEqual([]);
    });

    it('should fetch participant list', async () => {
        const mockChannel = {
            name: 'Common',
            url: 'common',
        };

        mockChannel.createParticipantListQuery = jest.fn();

        sbGetParticipantList.mockResolvedValue([{}, {}]);

        const dispatches = await Thunk(LobbyActions.getParticipantList).execute(
            mockChannel
        );

        expect(dispatches[0].getAction()).toEqual({
            type: MEMBER_LIST_SUCCESS,
            payload: [{}, {}],
        });
    });
});
