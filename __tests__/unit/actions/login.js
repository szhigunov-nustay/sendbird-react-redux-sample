import * as LoginActions from '../../../src/actions/login';
import {
    INIT_LOGIN,
    SIGNOUT,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
} from '../../../src/actions/types';

import { push } from 'react-router-redux';
import { Thunk } from 'redux-testkit';
import { sbConnect } from '../../../src/sendbird';
jest.mock('../../../src/sendbird');

describe('store/login/actions', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should init login screen', () => {
        expect(LoginActions.initLogin()).toEqual({ type: INIT_LOGIN });
    });

    it('should sign out account', async () => {
        const dispatches = await Thunk(LoginActions.signOut).execute();

        expect(dispatches[0].getType()).toEqual(SIGNOUT);
    });

    it('should sign in', async () => {
        sbConnect.mockResolvedValue({
            userId: 12345,
            nickname: 'testUser',
        });
        const dispatches = await Thunk(LoginActions.sendbirdLogin).execute({
            userId: 12345,
            nickname: 'testUser',
        });

        expect(dispatches[0].getAction()).toEqual({
            type: LOGIN_SUCCESS,
            payload: {
                userId: 12345,
                nickname: 'testUser',
            },
        });
        expect(dispatches[1].getAction()).toEqual(push('/'));
    });
});